<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\SteamAuthController;
use App\Http\Controllers\Home\HomeController;
use App\Http\Controllers\Lan\LanController;
use App\Http\Controllers\Lan\Material\LanMaterialController;
use App\Http\Controllers\Lan\Material\MaterialController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return redirect()->route('users.lan');
});
Route::get('/', function () {
    return redirect()->route('users.lan');
});

Route::get('auth/steam', [SteamAuthController::class, 'redirectToSteam'])->name('auth.steam');
Route::get('auth/steam/handle', [SteamAuthController::class, 'handle'])->name('auth.steam.handle');
Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('logout', [LoginController::class, 'logout'])->name('logout');

Route::resources([
    'lans'  => LanController::class,
]);

Route::prefix('users')->group(function () {
    Route::get('games', [HomeController::class, 'games'])->name('users.games');
    Route::get('games/wishlist', [HomeController::class, 'wishlist'])->name('users.games.wishlist');
    Route::get('lan', [HomeController::class, 'lan'])->name('users.lan');
    Route::get('data/refresh', [HomeController::class, 'refreshDataUser'])->name('users.data.refresh');
});

Route::prefix('lans')->group(function () {
    Route::prefix('player')->group(function () {
        Route::post('search', [LanController::class, 'searchPlayer'])->name('lans.player.search');
        Route::post('vote/up', [LanController::class, 'upvoteGame'])->name('lans.player.vote.up');
        Route::post('vote/down', [LanController::class, 'downvoteGame'])->name('lans.player.vote.down');
        Route::post('points/add', [LanController::class, 'addPoint'])->name('lans.player.points.add');
        Route::post('points/remove', [LanController::class, 'removePoint'])->name('lans.player.points.remove');
        Route::post('points/reset', [LanController::class, 'resetPoints'])->name('lans.player.points.reset');
        Route::post('', [LanController::class, 'players'])->name('lans.players');
    });
});

Route::resource('materials', MaterialController::class);
Route::resource('lan_materials', LanMaterialController::class);
