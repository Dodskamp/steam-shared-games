<?php

namespace Database\Factories;

use App\Models\Game;
use Illuminate\Database\Eloquent\Factories\Factory;

class GameFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Game::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'appid' => $this->faker->randomNumber(7, $strict = false),
            'name' => $this->faker->word(),
            'img_icon_url' => $this->faker->imageUrl($width = 640, $height = 480),
            'provider' => 'STEAM',
            'discount_percent' => $this->faker->numberBetween($min = 0, $max = 100),
            'initial_formatted' => 'CHF ' . $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            'final_formatted' => 'CHF ' . $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            'provider_updated_at' => now()
        ];
    }
}
