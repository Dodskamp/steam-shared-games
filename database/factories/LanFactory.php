<?php

namespace Database\Factories;

use App\Models\Lan;
use Illuminate\Database\Eloquent\Factories\Factory;

class LanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Lan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'date_start' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'date_end' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'description' => $this->faker->text($maxNbChars = 50)
        ];
    }
}
