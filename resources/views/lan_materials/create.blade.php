@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Add material to LAN for User</h1>
        <form action="{{ route('lan_materials.store') }}" method="POST">
            @csrf
            <fieldset>
                <div class="form-group">
                    <label for="lan_id">Select LAN</label>
                    <select class="form-control" id="lan_id" name="lan_id">
                        <option value>...</option>
                        @foreach($lans as $l)
                            <option value="{{ $l->id }}" @if(isset($lan) && $l->id == $lan->id)selected @endif>{{ $l->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="material_id">Select Material</label>
                    <select class="form-control" id="material_id" name="material_id">
                        <option value>...</option>
                        @foreach($materials as $m)
                            <option value="{{ $m->id }}" @if(isset($material) && $m->id == $material->id)selected @endif>{{ $m->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="user_id">Select User</label>
                    <select class="form-control" id="user_id" name="user_id">
                        <option value>...</option>
                        @isset($lan)
                            @foreach ($lan->users as $u)
                                <option value="{{ $u->id }}" @if(isset($user) && $u->id == $user->id)selected @endif>{{ $u->name }}</option>
                            @endforeach
                        @endisset
                    </select>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <label for="quantity">Quantity</label>
                            <input type="number" class="form-control" id="quantity" value="{{ old('quantity') }}" name="quantity" autocomplete="off">
                        </div>
                        <div class="col">
                            <label for="unit">Unit</label>
                            <input type="text" class="form-control" id="unit" value="{{ old('unit') }}" name="unit" autocomplete="off">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary submit">Submit</button>
            </fieldset>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $('#lan_id').on('change', function() {
            if($('#lan_id').val()) {
                let lan_select = $('#lan_id');
                let lan_id = lan_select.val();
                $.ajax({
                    url: "{{ route('lans.players') }}",
                    data: {
                        'lan_id': lan_id
                    },
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                })
                .done(function(data) {
                    var el = $("#user_id");
                    el.empty(); // remove old options
                    data.forEach(function(index, option) {
                        $option = $("<option></option>")
                            .attr("value", index.id)
                            .text(index.name);
                        el.append($option);
                    });
                })
            } else {
                var el = $("#user_id");
                el.empty(); // remove old options
                $option = $("<option></option>")
                    .attr("value", null)
                    .text("...");
                el.append($option);
            }
        });
    </script>
@endsection
