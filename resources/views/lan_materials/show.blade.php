@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>
            {{ $lanMaterial->material->name }} <a href="{{ route('lan_materials.edit', $lanMaterial) }}"><small><i class="fas fa-pencil-alt"></i></small></a>
            <small class="text-muted">{{ $lanMaterial->lan->name }} -> {{ ucfirst($lanMaterial->lan->date_start->translatedFormat('d F Y')) }} to {{ ucfirst($lanMaterial->lan->date_end->translatedFormat('d F Y')) }}</small>
        </h3>
        <div class="row">
            <div class="col-12">
                {{ $lanMaterial->quantity }} {{ $lanMaterial->unit }}
            </div>
            <div class="col-12">
                Taken by : {{ $lanMaterial->user->name }}
            </div>
        </div>
@endsection
