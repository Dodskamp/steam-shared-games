@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit lan material</h1>
        <form action="{{ route('lan_materials.update', $lanMaterial) }}" method="POST">
            <input type="hidden" name="_method" value="PATCH">
            @csrf
            <fieldset>
                <div class="form-group">
                    <label for="user_id">Select User</label>
                    <select class="form-control" id="user_id" name="user_id">
                        @forelse ($lan->users as $u)
                            <option value="{{ $u->id }}"
                                    @if($u->id == $lanMaterial->user_id)selected @endif>{{ $u->name }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <label for="quantity">Quantity</label>
                            <input type="number" class="form-control" id="quantity" value="{{ $lanMaterial->quantity }}"
                                   name="quantity" autocomplete="off">
                        </div>
                        <div class="col">
                            <label for="unit">Unit</label>
                            <input type="text" class="form-control" id="unit" value="{{ $lanMaterial->unit }}" name="unit"
                                   autocomplete="off">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary submit">Submit</button>
            </fieldset>
        </form>
    </div>
@endsection
