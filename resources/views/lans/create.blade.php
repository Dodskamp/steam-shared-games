@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Create a new LAN</h1>
    <form action="{{ route('lans.store') }}" method="POST">
        @csrf
        <fieldset>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" aria-describedby="name" value="{{ old('name') }}" name="name" placeholder="Name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description" rows="3">{{ old('description') }}</textarea>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="date_start">Date Start</label>
                        <input type="text" class="form-control date-picker" id="date_start" value="{{ old('date_start') }}" name="date_start" autocomplete="off">
                    </div>
                    <div class="col">
                        <label for="date_end">Date End</label>
                        <input type="text" class="form-control date-picker" id="date_end" value="{{ old('date_end') }}" name="date_end" autocomplete="off">
                    </div>
                </div>
            </div>

            <h4>Players : </h4>
            <ul id="players" style="padding-left: 0;"></ul>

            <label for="friendslist">Friends List</label>
            <ul id="friendslist" style="padding-left: 0;">
                @foreach($friendslist as $friend)
                <li class="list-group-item">
                    <span><img src="{{ $friend['avatar'] }}" alt="{{ $friend['personaname'] }}"> {{ $friend['personaname'] }}</span>
                    <a class="add_friend" data-friend-steamid="{{ $friend['steamid'] }}"><i class="fas fa-user-plus" style="float: right;"></i></a>
                </li>
                @endforeach
            </ul>

            <div class="form-group">
                <label for="user">Add player</label>
                <div class="input-group mb-3">
                    <input type="text" class="form-control" id="user" name="user" placeholder="Player's name" autocomplete="off" aria-label="Add player">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" id="reset" type="button"><i class="fas fa-times"></i></button>
                    </div>
                </div>
            </div>
            <ul class="list-group"></ul>
            <br>
            <div id="search"></div>
            <input type="hidden" name="players" id="players_value" value="" />
            <button type="submit" class="btn btn-primary submit">Submit</button>
        </fieldset>
    </form>
</div>
@endsection

@section('scripts')
<script>
    $('.submit').on('click', function() {
        showLoading();
    });

    $('#date_start').datepicker({
        firstDay: 1,
        format: 'yyyy-mm-dd',
    });

    $('#date_end').datepicker({
        firstDay: 1,
        format: 'yyyy-mm-dd',
    });

    function debounce(callback, wait) {
        let timeout;
        return (...args) => {
            clearTimeout(timeout);
            timeout = setTimeout(function() {
                callback.apply(this, args);
            }, wait);
        };
    }

    $('#user').keyup(debounce(() => {
        let username = $('#user').val();
        $('.list-group').css('display', 'block');
        if (username.length == 0) {
            $('.list-group').css('display', 'none');
        } else {
            $.ajax({
                url: "{{ route('lans.player.search') }}",
                method: "POST",
                data: {
                    user: username
                },
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                success: function(data) {
                    $('.list-group').html(data);
                }
            });
        }
    }, 500));

    function reset() {
        $('#user').val('');
        $('#user').prop('disabled', false);
    }

    let players = [];

    $(document).on('click', '.search', function() {
        let username = $(this).text();
        let avatar = $(this).children().attr('src');
        $('#user').val(username);
        $('.list-group').css('display', 'none');

        // on save les ids des users
        let id = $(this).data("value");
        players.push(id);
        $('#players_value').val(players);

        $('#players').append(`
            <li class="list-group-item">
                <span><img src="` + avatar + `" alt="` + username + `"> ` + username + `</span>
                <a class="delete_player" data-player-id="` + id + `"><i class="fas fa-times" style="float: right;"></i></a>
            </li>
        `);
        reset();
    });

    $(document).on('click', '.add_friend', function() {
        let friend_steamid = $(this).data('friend-steamid');
        let username = $(this).parent().children('span').text();
        let avatar = $(this).parent().children('span').children('img').attr('src');
        players.push(friend_steamid);
        $('#players_value').val(players);
        $('#players').append(`
            <li class="list-group-item">
                <span><img src="` + avatar + `" alt="` + username + `"> ` + username + `</span>
                <a class="delete_player" data-player-id="` + friend_steamid + `"><i class="fas fa-times" style="float: right;"></i></a>
            </li>
        `);
        $(this).parent().remove();
    });

    $(document).on('click', '.delete_player', function() {
        let player_id = $(this).data('player-id');
        let username = $(this).parent().children('span').text();
        let avatar = $(this).parent().children('span').children('img').attr('src');
        players = $.grep(players, function(p) {
            return p !== player_id;
        });
        $('#players_value').val(players);
        $('#friendslist').append(`
            <li class="list-group-item">
                <span><img src="` + avatar + `" alt="` + username + `"> ` + username + `</span>
                <a class="add_friend" data-friend-steamid="` + player_id + `"><i class="fas fa-user-plus" style="float: right;"></i></a>
            </li>
        `);
        $(this).parent().remove();
    });

    $('#reset').on('click', function() {
        reset();
    })
</script>
@endsection
