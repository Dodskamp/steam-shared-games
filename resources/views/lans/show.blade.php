@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>
            {{ $lan->name }} <a href="{{ route('lans.edit', $lan) }}"><small><i
                        class="fas fa-pencil-alt"></i></small></a>
            <small class="text-muted">{{ ucfirst($lan->date_start->translatedFormat('d F Y')) }}
                to {{ ucfirst($lan->date_end->translatedFormat('d F Y')) }}</small>
        </h3>

        <p>
            {{ $lan->description }}
        </p>
        <p>
            @forelse($lan->users as $user)
                <a href="https://steamcommunity.com/profiles/{{ $user->steam_id }}/" target="_blank"><img
                        src="{{ $user->avatar }}" alt="{{ $user->name }}"></a>
            @empty
            @endforelse
        </p>

        <ul class="nav nav-tabs" id="playerTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="ranking-tab" data-toggle="tab" href="#ranking" role="tab"
                   aria-controls="ranking" aria-selected="true"><i class="fas fa-layer-group"></i> Ranking <span
                        class="badge badge-primary"></span></a>
            </li>
            @forelse($lan->users as $user)
                <li class="nav-item">
                    <a class="nav-link" id="{{ $user->name }}-tab" data-toggle="tab"
                       href="#{{ preg_replace('/[^A-Za-z0-9\-]/', '', $user->name) }}" role="tab"
                       aria-controls="{{ preg_replace('/[^A-Za-z0-9\-]/', '', $user->name) }}" aria-selected="true"><i
                            class="fas fa-user"></i> {{ $user->name }} <span
                            class="badge badge-primary">{{ count($user->games) }}</span></a>
                </li>
            @empty
            @endforelse
            <li class="nav-item">
                <a class="nav-link" id="shared-games-tab" data-toggle="tab" href="#shared-games" role="tab"
                   aria-controls="shared-games" aria-selected="true"><i class="fab fa-steam"></i> Shared games <span
                        class="badge badge-primary">{{ count($sharedGames) }}</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="shared-games-wishlist-tab" data-toggle="tab" href="#shared-games-wishlist"
                   role="tab" aria-controls="shared-games-wishlist" aria-selected="true"><i class="fab fa-steam"></i>
                    Shared wishlist's games <span
                        class="badge badge-primary">{{ count($wishlistSharedGames) }}</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="almost-shared-games-tab" data-toggle="tab" href="#almost-shared-games"
                   role="tab" aria-controls="almost-shared-games" aria-selected="true"><i class="fab fa-steam"></i>
                    Almost Shared games <span class="badge badge-primary">{{ count($almostSharedGames) }}</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="materials-tab" data-toggle="tab" href="#materials" role="tab"
                   aria-controls="materials" aria-selected="true"> <i class="fas fa-gamepad"></i> Materials <span
                        class="badge badge-primary">{{ count($lanMaterials) }}</span></a>
            </li>
        </ul>

        <div class="tab-content">
            @forelse($lan->users as $user)
                <div class="tab-pane" id="{{ preg_replace('/[^A-Za-z0-9\-]/', '', $user->name) }}" role="tabpanel"
                     aria-labelledby="{{ preg_replace('/[^A-Za-z0-9\-]/', '', $user->name) }}-tab">
                    <div class="row">
                        @forelse($user->games->sortBy('name') as $game)
                            <div class="col-12 col-sm-3">
                                <div class="card mb-3">
                                    <h3 class="card-header" style="font-size: 1.1em;"><a
                                            href="https://store.steampowered.com/app/{{ $game->appid }}"
                                            target="_blank">{{ $game->name }}</a></h3>
                                    <img style="width: 100%; display: block;"
                                         src="http://media.steampowered.com/steamcommunity/public/images/apps/{{ $game->appid }}/{{ $game->img_icon_url }}.jpg"
                                         alt="{{ $game->name }}">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">Playtime : {{ $game->pivot->playtime_forever }}
                                            min
                                        </li>
                                        @if($game->discount_percent == null || $game->discount_percent == 0)
                                            <li class="list-group-item">Price : {{ $game->final_formatted }}</li>
                                        @else
                                            <li class="list-group-item">Discount : {{ $game->discount_percent }} %</li>
                                            <li class="list-group-item">Initial price
                                                : {{ $game->initial_formatted }}</li>
                                            <li class="list-group-item">Final Price : {{ $game->final_formatted }}</li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
            @empty
            @endforelse
            <div class="tab-pane" id="shared-games" role="tabpanel" aria-labelledby="shared-games-tab">
                <div class="row">
                    @forelse($sharedGames as $game)
                        <div class="col-12 col-sm-3">
                            <div class="card mb-3">
                                <h3 class="card-header" style="font-size: 1.1em;"><a
                                        href="https://store.steampowered.com/app/{{ $game->appid }}"
                                        target="_blank">{{ $game->name }}</a></h3>
                                <img style="width: 100%; display: block;"
                                     src="http://media.steampowered.com/steamcommunity/public/images/apps/{{ $game->appid }}/{{ $game->img_icon_url }}.jpg"
                                     alt="{{ $game->name }}">
                                <ul class="list-group list-group-flush">
                                    @if($lan->date_end > now())
                                        <li class="list-group-item">
                                            <div class="vote circle">
                                                <div class="increment up"></div>
                                                <div class="increment down"></div>
                                                <div id="game_vote_{{ $game->appid }}" class="count"
                                                     data-lan-id="{{ $lan->id }}"
                                                     data-game-id="{{ $game->appid }}">{{ App\Models\Vote::where('lan_id', $lan->id)->where('game_id', $game->appid)->count() }}</div>
                                            </div>
                                        </li>
                                    @endif
                                    @if($game->discount_percent == null || $game->discount_percent == 0)
                                        <li class="list-group-item">Price : {{ $game->final_formatted }}</li>
                                    @else
                                        <li class="list-group-item">Discount : {{ $game->discount_percent }} %</li>
                                        <li class="list-group-item">Initial price : {{ $game->initial_formatted }}</li>
                                        <li class="list-group-item">Final Price : {{ $game->final_formatted }}</li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    @empty
                    @endforelse
                </div>
            </div>
            <div class="tab-pane" id="shared-games-wishlist" role="tabpanel"
                 aria-labelledby="shared-games-wishlist-tab">
                <div class="row">
                    @forelse($wishlistSharedGames as $game)
                        <div class="col-12 col-sm-3">
                            <div class="card mb-3">
                                <h3 class="card-header" style="font-size: 1.1em;"><a
                                        href="https://store.steampowered.com/app/{{ $game->appid }}"
                                        target="_blank">{{ $game->name }}</a></h3>
                                <img style="width: 100%; display: block;" src="{{ $game->img_icon_url }}"
                                     alt="{{ $game->name }}">
                                <ul class="list-group list-group-flush">
                                <!--
                            @if($lan->date_end > now())
                                    <li class="list-group-item">
                                        <div class="vote circle">
                                            <div class="increment up"></div>
                                            <div class="increment down"></div>
                                            <div id="game_vote_{{ $game->appid }}" class="count" data-lan-id="{{ $lan->id }}" data-game-id="{{ $game->appid }}">{{ App\Models\Vote::where('lan_id', $lan->id)->where('game_id', $game->appid)->count() }}</div>
                                </div>
                            </li>
                            @endif
                                    -->
                                    <li class="list-group-item">{!! $game->final_formatted !!}</li>
                                    <li class="list-group-item">
                                        @forelse ($game->tags as $tag)
                                            <span class="badge badge-secondary">{{ $tag }}</span>
                                        @empty
                                        @endforelse
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @empty
                        <div class="alert alert-secondary" role="alert">
                            No shared wishlist's games yet or one profile is private
                        </div>
                    @endforelse
                </div>
            </div>
            <div class="tab-pane" id="almost-shared-games" role="tabpanel" aria-labelledby="shared-games-tab">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" name="missing" id="missing" placeholder="Max missing games"
                           aria-label="Max missing games" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <a id="missing_link" href="{{ route('lans.show', ['lan' => $lan->id, 'missing' => 1]) }}"><span
                                class="input-group-text" id="basic-addon2">GO</span></a>
                    </div>
                </div>
                <div class="row">
                    @forelse($almostSharedGames as $game)
                        <div class="col-12 col-sm-3">
                            <div class="card mb-3">
                                <h3 class="card-header" style="font-size: 1.1em;"><a
                                        href="https://store.steampowered.com/app/{{ $game->appid }}"
                                        target="_blank">{{ $game->name }}</a></h3>
                                <img style="width: 100%; display: block;"
                                     src="http://media.steampowered.com/steamcommunity/public/images/apps/{{ $game->appid }}/{{ $game->img_icon_url }}.jpg"
                                     alt="{{ $game->name }}">
                                <ul class="list-group list-group-flush">
                                    @if($game->discount_percent == null || $game->discount_percent == 0)
                                        <li class="list-group-item">Price : {{ $game->final_formatted }}</li>
                                    @else
                                        <li class="list-group-item">Discount : {{ $game->discount_percent }} %</li>
                                        <li class="list-group-item">Initial price : {{ $game->initial_formatted }}</li>
                                        <li class="list-group-item">Final Price : {{ $game->final_formatted }}</li>
                                    @endif
                                    <li class="list-group-item">
                                        Missing :
                                        @foreach($lan->users as $player)
                                            @if(!$game->users->contains($player))
                                                <a href="https://steamcommunity.com/profiles/{{ $player->steam_id }}/"
                                                   target="_blank"><img src="{{ $player->avatar }}"
                                                                        alt="{{ $player->name }}"></a>
                                            @endif
                                        @endforeach
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @empty
                    @endforelse
                </div>
            </div>
            <div class="tab-pane active" id="ranking" role="tabpanel" aria-labelledby="ranking-tab">
                <table class="table table-hover">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Points</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $i = 1
                    @endphp
                    @foreach($lan->users()->orderBy('pivot_points', 'desc')->get() as $player)
                        <tr>
                            <th scope="row">{{ $i }}</th>
                            <td><img src="{{ $player->avatar }}" alt="{{ $player->name }}"> {{ $player->name }}</td>
                            <td id="points_{{ $player->id }}" class="points">{{ $player->pivot->points ?? 0 }}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="actions">
                                    <button type="button" class="btn btn-secondary remove_point"
                                            data-user-id="{{ $player->id }}" data-lan-id="{{ $lan->id }}"><i
                                            class="fas fa-minus"></i></button>
                                    <button type="button" class="btn btn-primary add_point"
                                            data-user-id="{{ $player->id }}" data-lan-id="{{ $lan->id }}"><i
                                            class="fas fa-plus"></i></button>
                                </div>
                            </td>
                        </tr>
                        @php
                            $i++
                        @endphp
                    @endforeach
                    </tbody>
                </table>
                <button class="btn btn-danger reset" data-lan-id="{{ $lan->id }}"><i class="fas fa-bomb"></i> Reset
                    score
                </button>
            </div>
            <div class="tab-pane" id="materials" role="tabpanel" aria-labelledby="materials-tab">
                <table class="table table-hover">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Material</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Unit</th>
                        <th scope="col">Who</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($lanMaterials as $lanMaterial)
                        <tr>
                            @if(isset($lanMaterial->material))
                                <th scope="row">{{ $lanMaterial->material->name }}</th>
                                <td>{{ $lanMaterial->quantity }}</td>
                                <td>{{ $lanMaterial->unit }}</td>
                                <td>{{ $lanMaterial->user->name }}</td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="actions">
                                        <a href="{{ route('lan_materials.edit', $lanMaterial) }}" type="button"
                                           class="btn btn-primary"><i class="fas fa-pencil-alt"></i></a>
                                        <form action="{{ route('lan_materials.destroy', $lanMaterial) }}" type="button"
                                              class="btn btn-danger" method="POST" style="float: right;">
                                            {{ method_field('DELETE') }}
                                            @csrf
                                            <a onclick="this.closest('form').submit();return false;"><i
                                                    class="fas fa-trash"></i></a>
                                        </form>
                                    </div>
                                </td>
                            @endif
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>
                <a href="{{ route('lan_materials.create', ['lan_id' => $lan->id]) }}" class="btn btn-primary">Link a
                    material</a>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {

            $('#missing').on('keyup', function () {
                let missing_nb = parseInt($('#missing').val());
                if (missing_nb >= parseInt(`{{ count($lan->users) }}`)) {
                    missing_nb = parseInt($('#missing').val(parseInt(`{{ count($lan->users) }}`) - 1));
                    alert('Must not exceed or be equal to the number of players in the LAN ({{ count($lan->users) }})');
                } else {
                    let hrefBefore = $('#missing_link').attr('href');
                    let url = new URL(hrefBefore);
                    url.searchParams.set("missing", missing_nb);
                    $('#missing_link').attr('href', url);
                }
            });

            $(".reset").click(function () {
                if (confirm('Are you sure ?')) {
                    let lan = $(this).data('lan-id');

                    $.ajax({
                        url: "{{ route('lans.player.points.reset') }}",
                        type: 'POST',
                        data: {
                            'lan': lan,
                        },
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        }
                    })
                        .done(function (data) {
                            $('table tr td.points').each(function (a) {
                                $(this).html(0);
                            });
                        })
                }
            });

            $(".add_point").click(function () {
                let user = $(this).data('user-id');
                let lan = $(this).data('lan-id');

                $.ajax({
                    url: "{{ route('lans.player.points.add') }}",
                    type: 'POST',
                    data: {
                        'user': user,
                        'lan': lan,
                    },
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                })
                    .done(function (data) {
                        $("#points_" + user).html(data);
                    })
            });

            $(".remove_point").click(function () {
                let user = $(this).data('user-id');
                let lan = $(this).data('lan-id');

                $.ajax({
                    url: "{{ route('lans.player.points.remove') }}",
                    type: 'POST',
                    data: {
                        'user': user,
                        'lan': lan,
                    },
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                })
                    .done(function (data) {
                        $("#points_" + user).html(data);
                    })
            });

            $(".increment").click(function () {
                let game = $("~ .count", this).data('game-id');
                let lan = $("~ .count", this).data('lan-id');
                var count = parseInt($("~ .count", this).text());

                if ($(this).hasClass("up")) {
                    $.ajax({
                        url: "{{ route('lans.player.vote.up') }}",
                        type: 'POST',
                        data: {
                            'game': game,
                            'lan': lan,
                        },
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        }
                    })
                        .done(function (data) {
                            if (data) {
                                count = count + 1;
                                $('#game_vote_' + game).text(count);
                            }
                        })
                } else {
                    $.ajax({
                        url: "{{ route('lans.player.vote.down') }}",
                        type: 'POST',
                        data: {
                            'game': game,
                            'lan': lan,
                        },
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                    })
                        .done(function (data) {
                            if (data === '1') {
                                count = count - 1;
                                $('#game_vote_' + game).text(count);
                            }
                        })
                }

                $(this).parent().addClass("bump");

                setTimeout(function () {
                    $(this).parent().removeClass("bump");
                }, 400);
            });
        });
    </script>
@endsection
