@extends('layouts.app')

@section('content')
<div class="container">
    <table class="table table-hover">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Material</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse($materials as $material)
            <tr>
                <th scope="row">{{ $material->name }}</th>
                <td>
                    <div class="btn-group" role="group" aria-label="actions">
                        <a href="{{ route('materials.edit', $material) }}" type="button" class="btn btn-primary"><i class="fas fa-pencil-alt"></i></a>
                        <form action="{{ route('materials.destroy', $material) }}" type="button" class="btn btn-danger" method="POST" style="float: right;">
                            {{ method_field('DELETE') }}
                            @csrf
                            <a onclick="this.closest('form').submit();return false;"><i class="fas fa-trash"></i></a>
                        </form>
                    </div>
                </td>
            </tr>
            @empty
            @endforelse
        </tbody>
    </table>
    <a href="{{ route('materials.create') }}" class="btn btn-primary">Create a LAN material</a>
</div>
@endsection
