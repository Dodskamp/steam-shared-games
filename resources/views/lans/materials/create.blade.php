@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Create material</h1>
    <form action="{{ route('materials.store') }}" method="POST">
        @csrf
        <fieldset>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" aria-describedby="name" name="name" placeholder="Material name">
            </div>
            <button type="submit" class="btn btn-primary submit">Submit</button>
        </fieldset>
    </form>
</div>
@endsection
