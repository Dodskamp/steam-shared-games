@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Your LAN</h2>
    <a href="{{ route('lans.create') }}" class="btn btn-primary">Create a new LAN</a>
    <br><br>
    <div class="row">
        @forelse($lans->sortBy('name') as $lan)
        <div class="col-12 col-sm-4">
            <div class="card mb-3">
                <h3 class="card-header" style="font-size: 1.1em;">{{ $lan->name }}
                    <a href="{{ route('lans.edit', $lan) }}"><small><i class="fas fa-pencil-alt"></i></small></a>
                    <form action="{{ route('lans.destroy', $lan) }}" class="d-inline-block" method="POST" style="float: right;">
                        {{ method_field('DELETE') }}
                        @csrf
                        <a onclick="this.closest('form').submit();return false;"><i class="fas fa-times"></i></a>
                    </form>
                </h3>
                <div class="card-body">
                    <p>{!! $lan->description ?? '<i>No description yet</i>' !!}</p>
                </div>
                <ul class="list-group list-group-flush">
                    @forelse($lan->users as $user)
                    <li class="list-group-item"><img src="{{ $user->avatar }}" alt="{{ $user->name }}"> <a href="https://steamcommunity.com/profiles/{{ $user->steam_id }}/" target="_blank">
                            {{ $user->name }}</a></li>
                    @empty
                    @endforelse
                </ul>
                <div class="card-body">
                    <a href="{{ route('lans.show', $lan) }}" class="btn btn-primary card-link show_lan">Show LAN</a>
                </div>
                <div class="card-footer text-muted">
                    {{ ucfirst($lan->date_start->translatedFormat('d F Y')) }} to {{ ucfirst($lan->date_end->translatedFormat('d F Y')) }}
                </div>
            </div>
        </div>
        @empty
        <div class="col-12 col-sm-4">
            <h4>Francis ?</h4>
            <blockquote class="blockquote">
                <p class="mb-0">WTF man ?! Where is Francis</p>
                <footer class="blockquote-footer">A very sad gamer :(</cite></footer>
            </blockquote>
        </div>
        @endforelse
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('.show_lan').on('click', function() {
        showLoading();
    });
</script>
@endsection
