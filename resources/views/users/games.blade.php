@extends('layouts.app')

@section('content')
<div class="container">
    <h2>My games ({{ count($games) }}) <a href="{{ route('users.data.refresh') }}" class="sync"><small><i class="fas fa-sync-alt"></i></small></a></h2>
    <div class="row">
        @forelse($games->sortBy('name') as $game)
        <div class="col-12 col-sm-3">
            <div class="card mb-3">
                <h3 class="card-header" style="font-size: 1.1em;"><a href="https://store.steampowered.com/app/{{ $game->appid }}" target="_blank">{{ $game->name }}</a></h3>
                <img style="width: 100%; display: block;" src="http://media.steampowered.com/steamcommunity/public/images/apps/{{ $game->appid }}/{{ $game->img_icon_url }}.jpg" alt="{{ $game->name }}">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Playtime : {{ $game->pivot->playtime_forever }} min</li>
                    @if($game->discount_percent == null || $game->discount_percent == 0)
                    <li class="list-group-item">Price : {{ $game->final_formatted }}</li>
                    @else
                    <li class="list-group-item">Discount : {{ $game->discount_percent }} %</li>
                    <li class="list-group-item">Initial price : {{ $game->initial_formatted }}</li>
                    <li class="list-group-item">Final Price : {{ $game->final_formatted }}</li>
                    @endif
                </ul>
            </div>
        </div>
        @empty
        @endforelse
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('.sync').on('click', function() {
        showLoading();
    });
</script>
@endsection
