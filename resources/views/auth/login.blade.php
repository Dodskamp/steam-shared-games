@extends('layouts.app')

@section('content')
<div class="container">
    <a href="{{ route('auth.steam') }}" class="steambutton"><span>Login With Steam</span>
        <div class="icon">
            <i class="fab fa-steam"></i>
        </div>
    </a>
</div>
@endsection
