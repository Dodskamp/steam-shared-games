<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\Steam\GameService;
use App\Services\Steam\UserService;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    protected $gameService;
    protected $userService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->gameService = new GameService();
        $this->userService = new UserService();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function games()
    {
        $data = [
            'games' => Auth::user()->games
        ];
        return view('users.games', $data);
    }

    public function lan()
    {
        $data = [
            'lans' => Auth::user()->lans,
        ];
        return view('users.lan', $data);
    }

    public function refreshDataUser()
    {
        $user = User::where('id', Auth::user()->id)->first();
        $user->getSteamGames();
        $user->getSteamAvatar();
        $this->gameService->getSteamGamesPriceData($user);
        return redirect()->route('users.games');
    }

    public function wishlist()
    {
        $wishlist = UserService::getSteamUserWishlist(Auth::user());
        $data = [
            'wishlist'  => $wishlist
        ];
        return view('users.wishlist', $data);
    }
}
