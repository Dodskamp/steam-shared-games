<?php

namespace App\Http\Controllers\Lan\Material;

use App\Http\Controllers\Controller;
use App\Models\LanMaterial;
use App\Models\Material;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materials = Material::all();
        return view('lans.materials.index', compact('materials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lans.materials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->validate([
            'name' => 'required|unique:materials',
        ]);
        $material = Material::create($input);
        return redirect()->route('materials.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Material $material
     * @return \Illuminate\Http\Response
     */
    public function show(Material $material)
    {
        return view('lans.materials.show', compact('material'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Material $material
     * @return \Illuminate\Http\Response
     */
    public function edit(Material $material)
    {
        return view('lans.materials.edit', compact('material'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Material $material
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Material $material)
    {
        $input = $request->validate([
            'name' => 'required|unique:materials',
        ]);
        $material = $material->update($input);
        return redirect()->route('materials.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Material $material
     * @return \Illuminate\Http\Response
     */
    public function destroy(Material $material)
    {
        $lanMaterials = LanMaterial::where('material_id', $material->id)->get();
        foreach ($lanMaterials as $lanMaterial)
        {
            $lanMaterial->delete();
        }
        $material->delete();
        return redirect()->back();
    }
}
