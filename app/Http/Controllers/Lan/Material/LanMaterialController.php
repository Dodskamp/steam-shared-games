<?php

namespace App\Http\Controllers\Lan\Material;

use App\Http\Controllers\Controller;
use App\Models\Lan;
use App\Models\LanMaterial;
use App\Models\Material;
use App\Models\User;
use http\Env\Response;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class LanMaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $materials = Material::all();
        return view('lan_materials.index', compact('materials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return View
     */
    public function create(Request $request): View
    {
        $lan_id = $request->get('lan_id');
        $material_id = $request->get('material_id');
        $user_id = $request->get('user_id');

        $lan = null;
        $material = null;
        $user = null;

        if (isset($lan_id))
            $lan = Lan::find($lan_id);
        if (isset($material_id))
            $material = Material::find($material_id);
        if (isset($user_id))
            $user = User::find($user_id);

        $materials = Material::all();
        $lans = Lan::all();
        return view('lan_materials.create', compact('materials', 'lans', $lan ? 'lan' : null, $material ? 'material' : null, $user ? 'user' : null));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $inputs = $request->validate([
            'lan_id' => 'required|exists:lans,id',
            'material_id' => 'required|exists:materials,id',
            'user_id' => 'required|exists:users,id',
            'quantity' => 'required',
            'unit' => 'nullable',
        ]);

        $lanMaterial = LanMaterial::create($inputs);

        return redirect()->route('lans.show', $lanMaterial->lan);
    }

    /**
     * Display the specified resource.
     *
     * @param  LanMaterial $lanMaterial
     * @return View
     */
    public function show(LanMaterial $lanMaterial): View
    {
        return view('lan_materials.show', compact('lanMaterial'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  LanMaterial $lanMaterial
     * @return View
     */
    public function edit(LanMaterial $lanMaterial): View
    {
        $lan = Lan::where('id', $lanMaterial->lan_id)->first();
        return view('lan_materials.edit', compact('lanMaterial', 'lan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  LanMaterial $lanMaterial
     * @return RedirectResponse
     */
    public function update(Request $request, LanMaterial $lanMaterial): RedirectResponse
    {
        $input = $request->validate([
            'user_id' => 'required|exists:users,id',
            'quantity' => 'required',
            'unit' => 'nullable',
        ]);
        $lanMaterial->update($input);

        return redirect()->route('lans.show', $lanMaterial->lan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  LanMaterial $lanMaterial
     * @return RedirectResponse
     */
    public function destroy(LanMaterial $lanMaterial): RedirectResponse
    {
        $lan = $lanMaterial->lan_id;
        $lanMaterial->delete();
        return redirect()->route('lans.show', $lan);
    }
}
