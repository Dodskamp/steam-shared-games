<?php

namespace App\Http\Controllers\Lan;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreLanRequest;
use App\Models\Lan;
use App\Models\LanMaterial;
use App\Models\User;
use App\Models\Vote;
use App\Services\Steam\GameService;
use App\Services\Steam\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LanController extends Controller
{

    private $userService;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->userService = new UserService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $friendlist = $this->userService->getSteamUserFriendlist(Auth::user());
        $data = [
            'friendslist' => $friendlist
        ];
        return view('lans.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLanRequest $request)
    {
        $lan = new Lan($request->all());
        $lan->save();
        $lan->users()->attach(Auth::user());
        $players = explode(',', $request->players);
        if (!empty($players[0])) {
            foreach ($players as $player) {
                $user = User::where('steam_id', $player)->first();
                // user doesn't exists in db
                if ($user === null) {
                    $steamUser = $this->userService->getSteamUsersData($player);

                    // create user
                    $user = new User();
                    $user->name = $steamUser[0]['personaname'];
                    $user->avatar = $steamUser[0]['avatar'];
                    $user->steam_id = $steamUser[0]['steamid'];
                    $user->save();

                    // load user's data
                    $user->getSteamGames();
                }
                if (!$lan->users->contains($user)) {
                    $lan->users()->attach($user);
                }
            }
        }

        // update all games price
        // $gameService = new GameService();
        // $gameService->getSteamGamesPriceData();

        return redirect()->route('lans.show', $lan);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lan  $lan
     * @return \Illuminate\Http\Response
     */
    public function show(Lan $lan, Request $request)
    {
        $missing = intval($request->missing);
        $gameService = new GameService();
        $sharedGames = $gameService->sharedGames($lan->users);
        $wishlistSharedGames = $gameService->sharedWishListGames($lan->users);
        $almostSharedGames = ($missing === 0) ? $gameService->almostSharedGames($lan) : $gameService->almostSharedGames($lan, $missing);
        $lanMaterials = LanMaterial::where('lan_id', $lan->id)->get();
        $data = [
            'lan' => $lan,
            'sharedGames' => $sharedGames->sortByDesc(function ($game) use ($lan) {
                return Vote::where('lan_id', $lan->id)->where('game_id', $game->appid)->count();
            }),
            'almostSharedGames' => $almostSharedGames,
            'lanMaterials' => $lanMaterials,
            'wishlistSharedGames' => $wishlistSharedGames
        ];
        return view('lans.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Lan  $lan
     * @return \Illuminate\Http\Response
     */
    public function edit(Lan $lan)
    {
        $data = [
            'friendslist' => $this->userService->getSteamUserFriendlist(Auth::user()),
            'lan' => $lan
        ];
        return view('lans.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lan  $lan
     * @return \Illuminate\Http\Response
     */
    public function update(StoreLanRequest $request, Lan $lan)
    {
        $lan->update($request->all());
        $lan->users()->detach();
        $lan->users()->attach(Auth::user());
        $players = explode(',', $request->players);
        if (!empty($players[0])) {
            foreach ($players as $player) {
                $user = User::where('steam_id', $player)->first();
                // user doesn't exists in db
                if ($user === null) {
                    $steamUser = $this->userService->getSteamUsersData($player);

                    // create user
                    $user = new User();
                    $user->name = $steamUser[0]['personaname'];
                    $user->avatar = $steamUser[0]['avatar'];
                    $user->steam_id = $steamUser[0]['steamid'];
                    $user->save();

                    // load user's data
                    $user->getSteamGames();
                }
                if (!$lan->users->contains($user)) {
                    $lan->users()->attach($user);
                }
            }
        }
        return redirect()->route('lans.show', $lan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lan  $lan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lan $lan)
    {
        $lan->delete();
        return redirect()->route('users.lan');
    }

    public function searchPlayer(Request $request)
    {
        $resultQuery = User::where('name', 'like', '%' . $request->user . '%')->get();
        $data = '';
        foreach ($resultQuery as $result) {
            $data .= '
                <li class="list-group-item contsearch">
                    <a href="javascript:void()" class="search" data-value="' . $result->steam_id . '"><img src="' . $result->avatar . '" alt="' . $result->name . '"></img> ' . $result->name . '</a>
                </li>
            ';
        }
        return $data;
    }

    public function upvoteGame(Request $request)
    {
        $vote = Vote::where('user_id', Auth::user()->id)->where('lan_id', $request->lan)->where('game_id', $request->game)->first();
        if ($vote === null) {
            Vote::create([
                'user_id' => Auth::user()->id,
                'lan_id' => $request->lan,
                'game_id' => $request->game
            ]);
            return true;
        } else {
            return false;
        }
    }

    public function downvoteGame(Request $request)
    {
        return Vote::where('user_id', Auth::user()->id)->where('lan_id', $request->lan)->where('game_id', $request->game)->delete();
    }

    public function addPoint(Request $request)
    {
        $points = DB::table('lan_user')
            ->where('lan_id', $request->lan)
            ->where('user_id', $request->user)
            ->get()[0]->points;

        if ($points === null) {
            $points = 1;
        } else {
            $points++;
        }

        DB::table('lan_user')
            ->where('lan_id', $request->lan)
            ->where('user_id', $request->user)
            ->update(['points'  => $points]);

        return $points;
    }

    public function removePoint(Request $request)
    {
        $points = DB::table('lan_user')
            ->where('lan_id', $request->lan)
            ->where('user_id', $request->user)
            ->get()[0]->points;

        if ($points === null || $points === 0) {
            $points = 0;
        } else {
            $points--;
        }

        DB::table('lan_user')
            ->where('lan_id', $request->lan)
            ->where('user_id', $request->user)
            ->update(['points'  => $points]);

        return $points;
    }

    public function resetPoints(Request $request)
    {
        $lan = Lan::where('id', $request->lan)->first();
        DB::table('lan_user')
            ->where('lan_id', $request->lan)
            ->whereIn('user_id', $lan->users->pluck('id')->toArray())
            ->update(['points'  => 0]);
    }

    public function players(Request $request)
    {
        $lan = Lan::find($request->lan_id);
        return $lan->users;
    }
}
