<?php

namespace App\Services\Steam;

use App\Models\Game;
use App\Models\Lan;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Throwable;

class GameService
{
    protected $steam_api_key;

    public function __construct()
    {
        $this->steam_api_key = env('STEAM_API_KEY');
    }

    public function getUserSteamGames(User $user)
    {
        $url = 'http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=' . $this->steam_api_key . '&steamid=' . $user->steam_id . '&include_appinfo=true&include_played_free_games=true&format=json';
        $response = Http::get($url);
        try {
            $responseGames = $response->json()['response']['games'];
            $user->games()->detach();
            foreach ($responseGames as $gameData) {
                $gameDataLocal['appid'] =  $gameData['appid'];
                $gameDataLocal['name'] =  $gameData['name'];
                $gameDataLocal['img_icon_url'] =  $gameData['img_icon_url'];
                $gameDataLocal['provider'] = 'STEAM';
                $game = Game::firstOrCreate($gameDataLocal);
                $user->games()->attach($game, [
                    'playtime_forever' => $gameData['playtime_forever'] ?? '',
                    'playtime_windows_forever' => $gameData['playtime_windows_forever'] ?? '',
                    'playtime_mac_forever' => $gameData['playtime_mac_forever'] ?? '',
                    'playtime_linux_forever' => $gameData['playtime_linux_forever'] ?? '',
                ]);
            }
        } catch (Throwable $e) {
            Log::error($e);
            report($e);
        }
    }

    public function getSteamGamesPriceData($user = null)
    {
        $gamesList = '';
        $user_games = $user->games ?? Game::all();
        foreach ($user_games as $game) {
            $gamesList .= $game->appid . ',';
        }
        $url = 'https://store.steampowered.com/api/appdetails?appids=' . $gamesList . '&cc=ch&filters=price_overview';
        $response = Http::get($url);
        try {
            foreach ($user_games as $game) {
                $data = $response->json()[$game->appid]['data']['price_overview'] ?? [];
                $game->update([
                    'discount_percent' => $data['discount_percent'] ?? null,
                    'initial_formatted' => $data['initial_formatted'] ?? null,
                    'final_formatted' => $data['final_formatted'] ?? null
                ]);
            }
        } catch (Throwable $e) {
            Log::error($e);
            report($e);
        }
    }

    public function sharedGames(Collection $users)
    {
        $all_games = collect();
        foreach ($users as $user) {
            $all_games->push($user->games);
        }
        return Game::whereIn('appid', $this->compareGames($all_games))->get();
    }

    public function sharedWishListGames(Collection $users)
    {
        $all_games = collect();
        foreach ($users as $user) {
            $all_games->push(UserService::getSteamUserWishlist(User::find($user->id)));
        }
        $commonGames = $this->compareGames($all_games);
        $collapse = $all_games->collapse()->unique();
        return $collapse->whereIn('appid', $commonGames);
    }

    public function moveOtherToTop($collection, $key, $item)
    {
        return $collection->reject(function ($value) use ($item, $key) {
            return $value[$key] == $item;
        })->prepend($collection->filter(function ($value) use ($item, $key) {
            return $value[$key] == $item;
        })[$item]);
    }

    public function compareGames(Collection $games)
    {
        $sharedGamesAppIds = $games->reduce(function ($intersection, $items) {
            $items = $items->pluck('appid');
            $intersection = $intersection ?? collect($items);
            return $intersection->intersect($items);
        }, null);
        //dd('sharedGamesAppIds', $sharedGamesAppIds);
        return $sharedGamesAppIds;
    }

    public function almostSharedGames(Lan $lan, $missing = 1)
    {
        // TODO: WORKING BUT MUST BE OPTIMIZED
        // MUST RETURN THE USER WITH THE MISSING GAME
        /**
         * Pousser les jeux sans les données de l'utilisateur pour comparaison :
         * $user = User::find(1);
         * $user->games->makeHidden('pivot');
         */
        $all_games = collect();
        foreach ($lan->users as $user) {
            $all_games = $all_games->merge($user->games->makeHidden('pivot'));
        }
        $almostSharedGames = collect();
        $all_games->groupBy('appid')->each(function ($x) use ($lan, $missing, $almostSharedGames) {
            if ($x->count() + $missing === count($lan->users)) {
                $almostSharedGames->push($x->first());
            }
        });
        return $almostSharedGames;
    }
}
