<?php

namespace App\Services\Steam;

use App\Models\Game;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Throwable;

class UserService
{
    protected $steam_api_key;

    protected $gameService;

    public function __construct()
    {
        $this->steam_api_key = env('STEAM_API_KEY');
        $this->gameService = new GameService();
    }

    public function getSteamUserFriendlist(User $user)
    {
        // get user friendslist
        $url = 'http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key=' . $this->steam_api_key . '&steamid=' . $user->steam_id . '&relationship=friend';
        $response = Http::get($url);

        // prepare list of steamid
        $usersList = '';
        try {
            foreach ($response->json()['friendslist']['friends'] as $friend) {
                $usersList .= $friend['steamid'] . ',';
            }
        } catch (Throwable $e) {
            Log::error($e);
            report($e);
        }

        // get users data from steam
        return $this->getSteamUsersData($usersList);
    }

    public function getSteamUsersData($steamidList)
    {
        $url = 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=' . $this->steam_api_key . '&steamids=' . $steamidList;
        $response = Http::get($url);

        $playerslist = collect();

        try {
            if ($response->json() !== null) {
                foreach ($response->json()['response']['players'] as $player) {
                    $playerslist->push([
                        'steamid'       => $player['steamid'],
                        'avatar'        => $player['avatar'],
                        'personaname'   => $player['personaname'],
                    ]);
                }
            }
        } catch (Throwable $e) {
            Log::error($e);
            report($e);
        }

        return $playerslist;
    }

    public static function getSteamUserWishlist(User $user): Collection
    {
        $url = 'https://store.steampowered.com/wishlist/profiles/' . $user->steam_id . '/wishlistdata';
        $response = Http::get($url);
        $wishlist = collect();
        try {
            foreach ($response->json() as $key => $wishGame) {
                if ($wishGame === 2) {
                    return $wishlist;
                }
                $game = new Game();
                $game->appid = $key;
                $game->name = $wishGame['name'];
                $game->img_icon_url = $wishGame['capsule'];
                $game->provider = 'STEAM_WISHLIST';
                $game->final_formatted = strval($wishGame['subs']['0']['discount_block'] ?? '');
                $game->tags = $wishGame['tags'];
                $wishlist->push($game);
            }
        } catch (Throwable $e) {
            Log::error($e);
            report($e);
        }
        return $wishlist;
    }
}
