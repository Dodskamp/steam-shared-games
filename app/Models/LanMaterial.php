<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LanMaterial extends Model
{
    use HasFactory;

    protected $fillable = [
        'lan_id',
        'material_id',
        'user_id',
        'quantity',
        'unit',
    ];

    public function lan(): BelongsTo
    {
        return $this->belongsTo(Lan::class);
    }

    public function material(): BelongsTo
    {
        return $this->belongsTo(Material::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
