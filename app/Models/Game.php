<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class Game extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'appid',
        'name',
        'img_icon_url',
        'provider',
        'discount_percent',
        'initial_formatted',
        'final_formatted',
        'provider_updated_at'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

    public function getSteamData()
    {
        $url = 'https://store.steampowered.com/api/appdetails?appids=' . $this->appid . '&cc=ch&filters=price_overview';
        $response = Http::get($url);
        $data = $response->json()[$this->appid]['data']['price_overview'] ?? [];
        $this->update([
            'discount_percent' => $data['discount_percent'] ?? null,
            'initial_formatted' => $data['initial_formatted'] ?? null,
            'final_formatted' => $data['final_formatted'] ?? null
        ]);
    }
}
