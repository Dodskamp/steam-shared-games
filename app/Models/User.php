<?php

namespace App\Models;

use App\Services\Steam\GameService;
use App\Services\Steam\UserService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Throwable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $gameService;

    protected $userService;

    public function __construct()
    {
        $this->gameService = new GameService();
        $this->userService = new UserService();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'steam_id',
        'avatar',
        'avatar',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function lans()
    {
        return $this->belongsToMany(Lan::class)->withPivot([
            'points'
        ])->orderByPivot('points');
    }

    public function games()
    {
        return $this->belongsToMany(Game::class)->withPivot([
            'playtime_forever',
            'playtime_windows_forever',
            'playtime_mac_forever',
            'playtime_linux_forever'
        ]);
    }

    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

    public function getSteamGames()
    {
        return $this->gameService->getUserSteamGames($this);
    }

    public function getSteamAvatar()
    {
        $steam_api_key = env('STEAM_API_KEY');
        $url = 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=' . $steam_api_key . '&steamids=' . $this->steam_id;
        $response = Http::get($url);
        try {
            $playerData = $response->json()['response']['players'][0];
            $this->update(['avatar' => $playerData['avatar']]);
        } catch (Throwable $e) {
            Log::error($e);
            report($e);
        }
    }
}
